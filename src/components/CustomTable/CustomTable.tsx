import { Table } from 'antd';
import { ColumnType } from 'antd/lib/table';
import { useEffect, useState } from 'react';
import CircularRating from '../Rating/CircularRating';
import SemiCircularSlider from '../Rating/SemiCircularSlider';
import styles from './CustomTable.module.scss';

interface TableData {
    key: string,
    name: string,
    account: string,
    avc: string,
    stage: number,
    date: string,
    score: string
};

const columns: ColumnType<TableData>[] = [
    {
        title: 'Deal Name',
        dataIndex: 'name',
        key: 'name',
        render: (text: string) => <a>{text}</a>,
    },
    {
        title: 'Account Name',
        dataIndex: 'account',
        key: 'account',
        responsive: ['md'],
    },
    {
        title: 'AVC',
        dataIndex: 'avc',
        key: 'avc'
    },
    {
        title: 'Stage',
        dataIndex: 'stage',
        key: 'stage',
        render: (rating: number) => CircularRating(rating),
    },
    {
        title: 'Close Date',
        dataIndex: 'date',
        key: 'date'
    },
    {
        title: 'Engagement Score',
        dataIndex: 'score',
        key: 'score',
        render: (score: string) => SemiCircularSlider(score),
    },
];

const data: TableData[] = [
    {
        key: '1',
        name: 'Global media sample',
        account: 'Citi Bank',
        avc: '$100,00.00',
        stage: 2.5,
        date: '03/04/2022',
        score: '90%',
    },
    {
        key: '2',
        name: '4 monks',
        account: 'PNB Bank',
        avc: '$100,00.00',
        stage: 1.5,
        date: '03/04/2022',
        score: '80%',
    }, {
        key: '3',
        name: 'Shareef',
        account: 'ICICI Bank',
        avc: '$100,00.00',
        stage: 3,
        date: '03/04/2022',
        score: '50%',
    }, {
        key: '4',
        name: 'Gokul',
        account: 'HDFC Bank',
        avc: '$100,00.00',
        stage: 4,
        date: '03/04/2022',
        score: '30%',
    },
];

const CustomTable = (props: any) => {
    const [dataState, setDataState] = useState(data);
    useEffect(() => {
        const filteredData = data.filter((item) => {
            return (
                item.name?.toLowerCase().includes(props.inputSearchParams.toLowerCase()) ||
                item.account?.toLowerCase().includes(props.inputSearchParams.toLowerCase()) ||
                item.date?.toLowerCase().includes(props.inputSearchParams.toLowerCase()));
        });
        if (filteredData) {
            setDataState(filteredData);
        }
    }, [props]);
    return (
        <Table<TableData> columns={columns} dataSource={dataState} />
    )
}

export default CustomTable;