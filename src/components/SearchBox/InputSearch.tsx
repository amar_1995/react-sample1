import { Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import styles from './InputSearch.module.scss';

const SearchParams = (props: any) => {
    const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        props.setInputSearchParams(e.target.value);
    }
    return (
        <div className={styles.search}>
            <div>
                <Input
                    bordered
                    size='large'
                    placeholder="Search by deals, accounts, contacts or ACV"
                    value={props.inputSearchParams}
                    onChange={onChange}
                    prefix={<SearchOutlined />}
                />
            </div>
        </div>
    );
}

export default SearchParams;