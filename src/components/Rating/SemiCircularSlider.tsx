import styles from './SemiCircularSlider.module.scss';

const SemiCircularSlider = (score: string) => {
    const degree = 45+parseInt(score)*1.8
    return (
        <div>
            <div className={styles.progress}>
                <div className={styles.barOverflow}>
                    <div className={styles.bar}
                        style={{
                            transform: `rotate(${degree}deg)`
                        }}
                    ></div>
                </div>
            </div> &nbsp;
            <span>
                {score}
            </span>
        </div>
    )
}

export default SemiCircularSlider;