
const CircleFilledColor = (props: any) => {
    return (
        <span key={props.keyProp} style={
            {
                height: '12px',
                width: '16px',
                marginRight: '2px',
                backgroundColor: props.color,
                borderRadius: '50%',
                display: 'inline-block'
            }}>
        </span >
    )
}

const CircularRating = (rating: number) => {
    let i = 5;
    const arr: JSX.Element[] = [];
    if (rating && rating > 0) {
        for (let x = 0; x < Math.floor(rating); x++) {
            arr.push(<CircleFilledColor color={'blue'} keyProp={'a'+x} />);
            i--;
        }
    }
    if (rating % 1 !== 0) {
        arr.push(<CircleFilledColor color={'#41A8FC'} keyProp={'a'+i} />);
        i--;
    }
    while (i > 0) {
        arr.push(<CircleFilledColor color={"#adb4ba"} keyProp={'b'+i}/>);
        i--;
    }
    return (
        <span>
            {arr} &nbsp;
        </span>
    );
};

export default CircularRating;