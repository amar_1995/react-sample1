import style from "./App.module.scss";
import InputSearch from './components/SearchBox/InputSearch';

import 'antd/dist/antd.css';
import CustomTable from "./components/CustomTable/CustomTable";
import { useState } from "react";


function App() {
  const [inputSearchParams, setInputSearchParams] = useState('');
  return (
    <div className={style.App}>
      <InputSearch inputSearchParams={inputSearchParams} setInputSearchParams={setInputSearchParams}/>
      <CustomTable inputSearchParams={inputSearchParams}/>
    </div>
  );
}

export default App;
